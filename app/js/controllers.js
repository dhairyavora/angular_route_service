'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', '$http',
    function($scope, $http) {
        $http.get('phones/phones.json').success(function(data) {
            $scope.phones = data;
        });
    
        $scope.orderProp = 'age';
    }
]);

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', '$route', '$http', '$timeout',
    function($scope, $routeParams, $route, $http, $timeout) {
        $scope.param = $route.current.param1;
        $scope.phoneName = $route.current.params.phoneName;
        $scope.phoneId = $routeParams.phoneId;
        $timeout(function(){
            $http.get('phones/'+$routeParams.phoneId+'.json').success(function(data) {
                $scope.phoneData = data;
            });    
        },3000);
    }
]); 